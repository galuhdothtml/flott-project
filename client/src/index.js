/* eslint prop-types: 0 */
import React from "react"; // eslint-disable-line no-unused-vars
import ReactDOM from "react-dom";
import { Provider } from "react-redux"; // eslint-disable-line no-unused-vars
import App from "~/route"; // eslint-disable-line no-unused-vars
import store from "./js/store";

const wrapper = document.getElementById("root"); // eslint-disable-line no-undef
wrapper // eslint-disable-line no-unused-expressions
  ? ReactDOM.render(
    <Provider store={store.getStore()}>
      <App />
    </Provider>, wrapper,
  )
  : false;
