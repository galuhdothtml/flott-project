import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { createLogger } from "redux-logger";
import configureMockStore from "redux-mock-store";
import reducers from "./reducers";

const loggerMiddleware = createLogger();

const getStore = () => {
  const middleWare = applyMiddleware(
    thunkMiddleware, // lets us dispatch() functions
    loggerMiddleware, // neat middleware that logs actions
  );

  return createStore(
    reducers,
    middleWare,
  );
};

const getMockStore = (state = {}) => {
  const middlewares = [thunkMiddleware];
  const mockStore = configureMockStore(middlewares);

  return mockStore(state);
};

const store = {
  getStore,
  getMockStore,
};

export default store;
