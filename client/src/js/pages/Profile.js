/* eslint prop-types: 0 */
import React from "react";
import InputText from "~/components/form/InputText";

class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "Halaman Profile",
    };
  }


  render() {
    const { title } = this.state;

    return (
      <div>
        <h1>{title}</h1>
        <InputText />
      </div>
    );
  }
}

export default Profile;
