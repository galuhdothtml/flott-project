/* eslint prop-types: 0 */
import React from "react";
import { Link } from "react-router-dom";
import InputText from "~/components/form/InputText";

class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      menu: [
        {
          id: "1",
          title: "Home",
          link: "home",
        },
        {
          id: "2",
          title: "Profile",
          link: "profile",
        },
      ],
    };
  }


  render() {
    const { menu } = this.state;

    return (
      <div className="temp-nav">
        <ul>
            {menu.map(x => (
                <li key={x.id}>
                    <Link to={`/${x.link}`}>{x.title}</Link>
                </li>
            ))}
        </ul>
      </div>
    );
  }
}

export default Profile;
