/* eslint prop-types: 0 */
import React from "react"; // eslint-disable-line no-unused-vars
import {
  BrowserRouter as Router, Route, Switch, Redirect,
} from "react-router-dom";
import AppNavigation from "./components/AppNavigation";
import asyncComponent from "./components/AsyncComponent";

require("../sass/styles.scss");

const RedirectToDashboard = () => (
  <Redirect
    to={{
      pathname: "/home",
    }}
  />
);
class App extends React.Component {
  render = () => (
    <Router>
      <Switch>
        <Route exact path="/auth/login" component={asyncComponent(() => import("../pages/Login"))} />
        <Route path="/">
          <React.Fragment>
            <AppNavigation />
            <Route exact path="/" component={RedirectToDashboard} />
            <Route path="/home" component={asyncComponent(() => import("../pages/Home"))} />
            <Route path="/profile" component={asyncComponent(() => import("../pages/Profile"))} />
          </React.Fragment>
        </Route>
      </Switch>
    </Router>
  )
}

export default App;
