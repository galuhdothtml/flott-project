/* eslint prop-types: 0 */
import React from "react";

class InputText extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "test input text",
    };
  }


  render() {
    const { title } = this.state;

    return (
      <div>
        <div>{title}</div>
      </div>
    );
  }
}

export default InputText;
